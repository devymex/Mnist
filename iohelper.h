#ifndef _IOHELPER_H
#define _IOHELPER_H

#include <vector>
#include <string>

#include "nntypes.h"

bool LoadImageSet(
		const std::string &strFileName,
		std::vector<float> &imgs,
		int *pImgCnt,
		int *pImgHeight,
		int *pImgWidth
		);

bool LoadLableSet(const std::string &strFileName, std::vector<BYTE> &lables);

#endif
