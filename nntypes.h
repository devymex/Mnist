
#ifndef NNTYPES_H_
#define NNTYPES_H_

#include <vector>

typedef unsigned char BYTE;
typedef std::vector<BYTE> IMGBUF;
typedef std::vector<float> IMGDATA;
typedef std::vector<float> OUTPUT;

#endif
