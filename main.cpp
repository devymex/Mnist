#include <opencv2/opencv.hpp>

#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>

#include "iohelper.h"
#include "train.h"

void ImgBuf2Data(const IMGBUF &imgBuf, IMGDATA &imgData)
{
	imgData.resize(imgBuf.size());
	for (int i = 0; i < (int)imgData.size(); ++i)
	{
		imgData[i] = (float)imgBuf[i] / 255.0f;
	}
}

template<typename _Ty>
void Permutation(const std::vector<int> &order, std::vector<_Ty> &ary, int nUnitSize)
{
	std::vector<_Ty> tmp(ary.size());
	for (int i = 0; i < (int)order.size(); ++i)
	{
		_Ty *pSrc = ary.data() + order[i] * nUnitSize;
		_Ty *pDst = tmp.data() + i * nUnitSize;
		std::memcpy(pDst, pSrc, nUnitSize * sizeof(_Ty));
	}
	tmp.swap(ary);
}

int main(int nArgCnt, char **ppArgs)
{
	if (nArgCnt < 2)
	{
		std::cout << "Please specify the data path!" << std::endl;
	}
	std::string strPath = ppArgs[1];
	if (strPath.back() != '/')
	{
		strPath.push_back('/');
	}

	std::string strTrainImgFn = strPath + "imgdb";
	std::string strTrainLabFn = strPath + "lables";

	std::vector<float> images;
	std::vector<BYTE> lables;

	int nImgCnt, nImgHeight, nImgWidth;
	if (!LoadImageSet(strTrainImgFn, images, &nImgCnt, &nImgHeight, &nImgWidth))
	{
		return -1;
	}
	if (!LoadLableSet(strTrainLabFn, lables))
	{
		return -1;
	}
	if (nImgCnt != (int)lables.size())
	{
		std::cout << "Number of images doesn't matched with lables" << std::endl;
		return -1;
	}

	std::vector<int> order(images.size() / (nImgHeight * nImgWidth));
	for (int i = 0; i < (int)order.size(); ++i)
	{
		order[i] = i;
	}
	std::random_shuffle(order.begin(), order.end());
	Permutation(order, images, nImgHeight * nImgWidth);
	Permutation(order, lables, sizeof(BYTE));

	NNSIZE size;
	size.imgCnt = nImgCnt;
	size.imgSize = nImgHeight * nImgWidth;
	size.hidSize = 16;
	size.outSize = 2;

	std::vector<int> randIdx(size.imgCnt);
	for (int i = 0; i < size.imgCnt; ++i)
	{
		randIdx[i] = i;
	}
	std::random_shuffle(randIdx.begin(), randIdx.end());
	std::vector<float> randImages(images.size());
	std::vector<BYTE> randLables(lables.size());
	for (int i = 0; i < size.imgCnt; ++i)
	{
		std::memcpy(
				randImages.data() + i * size.imgSize,
				images.data() + randIdx[i] * size.imgSize,
				size.imgSize * sizeof(float));
		randLables[i] = lables[randIdx[i]];
	}
	images = randImages;
	lables = randLables;

	NNWEIGHT outW;
	InitHostWeight(size, outW);
	Train(size, images.data(), lables.data(), outW);
	for (int i = 0; i < size.hidSize; ++i)
	{
		float *pBeg = outW.WH + i * size.imgSize;
		float *pEnd = pBeg + size.imgSize;
		float fMin = *std::min_element(pBeg, pEnd);
		float fMax = *std::max_element(pBeg, pEnd);
		float fScale = 255.0f / (fMax - fMin);

		std::vector<BYTE> wmap;
		for (int j = 0; j < size.imgSize; ++j)
		{
			BYTE val = (BYTE)((pBeg[j] - fMin) * fScale);
			wmap.push_back(val);
		}
		cv::Mat img(nImgHeight, nImgWidth, CV_8UC1, wmap.data());
		std::ostringstream oss;
		oss << strPath << i << ".png";
		cv::imwrite(oss.str(), img);
	}
	FreeHostWeight(outW);

	std::cout << "OK!" << std::endl;
	return 0;
}
