#include <iostream>
#include <vector>

#include "train.h"
#include "mytimer.h"

__device__ void MatMul_cuda(
		float *pOut, int nRows,
		const float *pLeft, int nLCols,
		const float *pRight, int nRCols)
{
	for (int r = 0; r < nRows; ++r)
	{
		for (int c = 0; c < nRCols; ++c)
		{
			float &fVal = pOut[r * nRCols + c];
			fVal = 0.0f;
			for (int k = 0; k < nLCols; ++k)
			{
				fVal += pLeft[r * nLCols + k] * pRight[k * nRCols + c];
			}
		}
	}
}

__device__ void MatAdd_cuda(
		float *pOut, int nRows, int nCols,
		const float *pLeft, const float *pRight)
{
	for (int i = 0; i < nRows * nCols; ++i)
	{
		pOut[i] = pLeft[i] + pRight[i];
	}
}

__device__ void MatSig_cuda(float *pOut, int nRows, int nCols, const float *pIn)
{
	for (int i = 0; i < nRows * nCols; ++i)
	{
		if (pIn[i] < -30)
		{
			pOut[i] = 0.0f;
		}
		else
		{
			pOut[i] = 1.0f / (1.0f + exp(-pIn[i]));
		}
	}
}

__device__ void MatMulVal_cuda(float *pOut, int nRows, int nCols,
		const float *pIn, float fVal)
{
	for (int i = 0; i < nRows * nCols; ++i)
	{
		pOut[i] = pIn[i] * fVal;
	}
}

__device__ void Cost(const NNSIZE &size, const NNBUFFER &idxBuf)
{
	*idxBuf.bufCost = 0.0f;
	for (int k = 0; k < size.outSize; ++k)
	{
		idxBuf.bufErr[k] = (float)(k == *(idxBuf.lables)) - idxBuf.bufOut[k];
		*idxBuf.bufCost += idxBuf.bufErr[k] * idxBuf.bufErr[k];
	}
	*idxBuf.bufCost /= 2.0f;
}

__device__ void UpdateResults(const NNSIZE &size, const NNBUFFER &idxBuf)
{
	float fMax = 0.0f;
	*idxBuf.results = 0;
	for (BYTE i = 0; i < size.outSize; ++i)
	{
		if (idxBuf.bufOut[i] > fMax)
		{
			fMax = idxBuf.bufOut[i];
			*idxBuf.results = i;
		}
	}
	*idxBuf.results = (*idxBuf.results == *idxBuf.lables);
}

__device__ void Proc(const NNSIZE &size, const NNWEIGHT &weights, const NNBUFFER &idxBuf)
{
	MatMul_cuda(idxBuf.bufHid, size.hidSize, weights.WH, size.imgSize, idxBuf.images, 1);
	MatAdd_cuda(idxBuf.bufHid, size.hidSize, 1, idxBuf.bufHid, weights.BH);
	MatSig_cuda(idxBuf.bufHid, size.hidSize, 1, idxBuf.bufHid);

	MatMul_cuda(idxBuf.bufOut, size.outSize, weights.WO, size.hidSize, idxBuf.bufHid, 1);
	MatAdd_cuda(idxBuf.bufOut, size.outSize, 1, idxBuf.bufOut, weights.BO);

	//SoftMax
	float fMax = 0.0f;
	for (int k = 0; k < size.outSize; ++k)
	{
		if (fMax < idxBuf.bufOut[k])
		{
			fMax = idxBuf.bufOut[k];
		}
	}
	
	float fSum = 0.0f;
	for (int k = 0; k < size.outSize; ++k)
	{
		idxBuf.bufOut[k] = exp(idxBuf.bufOut[k] - fMax);
		fSum += idxBuf.bufOut[k];
	}
	MatMulVal_cuda(idxBuf.bufOut, size.outSize, 1, idxBuf.bufOut, 1.0f / fSum);
}

__device__ void TrainImg(
	const NNSIZE &size,
	const NNWEIGHT &weights,
	const NNBUFFER &idxBuf,
	float fLearnRate)
{
	Proc(size, weights, idxBuf);
	UpdateResults(size, idxBuf);
	Cost(size, idxBuf);

	for (int k = 0; k < size.outSize; ++k)
	{
		//the delta^O
		idxBuf.bufErr[k] = idxBuf.bufErr[k] * idxBuf.bufOut[k] * (1.0f - idxBuf.bufOut[k]);
		float fdelta = fLearnRate * idxBuf.bufErr[k];
		for (int j = 0; j < size.hidSize; ++j)
		{
			idxBuf.deltaWO[k * size.hidSize + j] = fdelta * idxBuf.bufHid[j];
		}
		idxBuf.deltaBO[k] = fdelta;
	}

	// sum of w * delta
	MatMul_cuda(idxBuf.bufSum, 1, idxBuf.bufErr, size.outSize, weights.WO, size.hidSize);
	for (int j = 0; j < size.hidSize; ++j)
	{
		//the delta^H
		idxBuf.bufSum[j] = idxBuf.bufSum[j] * idxBuf.bufHid[j] * (1.0f - idxBuf.bufHid[j]);

		float fdelta = fLearnRate * idxBuf.bufSum[j];
		for (int i = 0; i < size.imgSize; ++i)
		{
			idxBuf.deltaWH[j * size.imgSize + i] = fdelta * idxBuf.images[i];
		}
		idxBuf.deltaBH[j] = fdelta;
	}
}

__device__ void GetIdxBuf(
	const NNSIZE &size,
	const NNBUFFER &allBuf,
	int idx,
	NNBUFFER &idxBuf)
{
	idxBuf.images = allBuf.images + idx * size.imgSize;
	idxBuf.lables = allBuf.lables + idx;
	idxBuf.results = allBuf.results + idx;

	idxBuf.deltaWH = allBuf.deltaWH + idx * size.hidSize * size.imgSize;
	idxBuf.deltaBH = allBuf.deltaBH + idx * size.hidSize;
	idxBuf.deltaWO = allBuf.deltaWO + idx * size.outSize * size.hidSize;
	idxBuf.deltaBO = allBuf.deltaBO + idx * size.outSize;
	
	idxBuf.bufHid = allBuf.bufHid + idx * size.hidSize;
	idxBuf.bufSum = allBuf.bufSum + idx * size.hidSize;
	idxBuf.bufOut = allBuf.bufOut + idx * size.outSize;
	idxBuf.bufErr = allBuf.bufErr + idx * size.outSize;
	
	idxBuf.bufCost = allBuf.bufCost + idx;
}

__global__ void TrainEpoch(NNSIZE size, NNWEIGHT weights, NNBUFFER allBuf, float fLearnRate)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	
	NNBUFFER idxBuf;
	GetIdxBuf(size, allBuf, idx, idxBuf);
	
	TrainImg(size, weights, idxBuf, fLearnRate);
}

//=============================================================================

#define CUDA_CHECK(ans) { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
	if (code != cudaSuccess) 
	{
		std::cerr << "CUDA Error: " << cudaGetErrorString(code) << " ";
		std::cerr << file << " " << line << std::endl;
		if (abort)
		{
			exit(code);
		}
   }
}

void _InitWeight(float *pDevMat, int nRows, int nCols)
{
	float fRange = 1.0f / sqrt((float)nCols);
	std::vector<float> rands(nRows * nCols);
	for (int i = 0; i < nRows * nCols; ++i)
	{
		float fRand = (float)(rand() * 2.0f) / (float)RAND_MAX - 1.0f;
		rands[i] = fRand * fRange;
	}
	cudaMemcpy(pDevMat, rands.data(),
		sizeof(float) * nRows * nCols, cudaMemcpyHostToDevice);
}

void InitWeight(const NNSIZE &size, NNWEIGHT &weights)
{
	cudaMalloc((void**)&weights.WH, sizeof(float) * size.hidSize * size.imgSize);
	_InitWeight(weights.WH, size.hidSize, size.imgSize);
	
	cudaMalloc((void**)&weights.BH, sizeof(float) * size.hidSize);
	cudaMemset(weights.BH, 0, sizeof(float) * size.hidSize);
	
	cudaMalloc((void**)&weights.WO, sizeof(float) * size.outSize * size.hidSize);
	_InitWeight(weights.WO, size.outSize, size.hidSize);

	cudaMalloc((void**)&weights.BO, sizeof(float) * size.outSize);
	cudaMemset(weights.BO, 0, sizeof(float) * size.outSize);
}

void InitHostWeight(const NNSIZE &size, NNWEIGHT &weights)
{
	weights.WH = new float[size.hidSize * size.imgSize];
	weights.BH = new float[size.hidSize];
	weights.WO = new float[size.outSize * size.hidSize];
	weights.BO = new float[size.outSize];
}

void FreeHostWeight(const NNWEIGHT &weights)
{
	delete[] weights.WH;
	delete[] weights.BH;
	delete[] weights.WO;
	delete[] weights.BO;
}

void InitBuffer(const NNSIZE &size, NNBUFFER &allBuf)
{
	int nBase = sizeof(float) * size.imgCnt;
	CUDA_CHECK(cudaMalloc((void**)&allBuf.images, nBase * size.imgSize));
	CUDA_CHECK(cudaMalloc((void**)&allBuf.lables, size.imgCnt));
	CUDA_CHECK(cudaMalloc((void**)&allBuf.results, size.imgCnt));
	
	CUDA_CHECK(cudaMalloc((void**)&allBuf.deltaWH, nBase * size.hidSize * size.imgSize));
	CUDA_CHECK(cudaMalloc((void**)&allBuf.deltaBH, nBase * size.hidSize));
	CUDA_CHECK(cudaMalloc((void**)&allBuf.deltaWO, nBase * size.outSize * size.hidSize));
	CUDA_CHECK(cudaMalloc((void**)&allBuf.deltaBO, nBase * size.outSize));
	
	CUDA_CHECK(cudaMalloc((void**)&allBuf.bufHid, nBase * size.hidSize));
	CUDA_CHECK(cudaMalloc((void**)&allBuf.bufSum, nBase * size.hidSize));
	CUDA_CHECK(cudaMalloc((void**)&allBuf.bufOut, nBase * size.outSize));
	CUDA_CHECK(cudaMalloc((void**)&allBuf.bufErr, nBase * size.outSize));
	
	CUDA_CHECK(cudaMalloc((void**)&allBuf.bufCost, nBase));
}

void FillData(const NNSIZE &size, const float *pImages,
	const BYTE *pLables, NNBUFFER &allBuf)
{
	CUDA_CHECK(cudaMemcpy(allBuf.images, pImages,
		sizeof(float) * size.imgCnt * size.imgSize, cudaMemcpyHostToDevice));
	CUDA_CHECK(cudaMemcpy(allBuf.lables, pLables, size.imgCnt, cudaMemcpyHostToDevice));
	CUDA_CHECK(cudaMemset(allBuf.results, 0, size.imgCnt));
}

void FreeBuffer(const NNBUFFER &allBuf)
{
	cudaFree(allBuf.images);
	cudaFree(allBuf.lables);
	cudaFree(allBuf.results);
	
	cudaFree(allBuf.deltaWH);
	cudaFree(allBuf.deltaBH);
	cudaFree(allBuf.deltaWO);
	cudaFree(allBuf.deltaBO);
	
	cudaFree(allBuf.bufHid);
	cudaFree(allBuf.bufSum);
	cudaFree(allBuf.bufOut);
	cudaFree(allBuf.bufErr);
	
	cudaFree(allBuf.bufCost);
}

void FreeWeight(const NNWEIGHT &weights)
{
	cudaFree(weights.WH);
	cudaFree(weights.BH);
	cudaFree(weights.WO);
	cudaFree(weights.BO);
}

void Precision(const NNSIZE &size, const NNBUFFER &allBuf,
	float *pCost, float *pPrec)
{
	float *pCostBuf = 0;
	BYTE *pResBuf = 0;
	
	CUDA_CHECK(cudaMallocHost((void**)&pCostBuf, sizeof(float) * size.imgCnt));
	CUDA_CHECK(cudaMallocHost((void**)&pResBuf, size.imgCnt));
	
	CUDA_CHECK(cudaMemcpy(pCostBuf, allBuf.bufCost,
		sizeof(float) * size.imgCnt, cudaMemcpyDeviceToHost));
	CUDA_CHECK(cudaMemcpy(pResBuf, allBuf.results,
		size.imgCnt, cudaMemcpyDeviceToHost));

	*pCost = 0;
	*pPrec = 0;
	for (int i = 0; i < size.imgCnt; ++i)
	{
		*pCost += pCostBuf[i];
		*pPrec += (float)pResBuf[i];
	}
	CUDA_CHECK(cudaFreeHost(pCostBuf));
	CUDA_CHECK(cudaFreeHost(pResBuf));
	*pCost /= size.imgCnt;
	*pPrec /= size.imgCnt;
}

void DumpBuf(float *p, int size)
{
	std::vector<float> buf(size);
	CUDA_CHECK(cudaMemcpy(buf.data(), p,
		sizeof(float) * size, cudaMemcpyDeviceToHost));
	for (int i = 0; i < size; ++i)
	{
		std::cout << i << ":" << buf[i] << std::endl;
	}
}

void FixWeight(float *weights, float *delta, int imgCnt, int weightSize)
{
	float *pSrc = 0;
	float *pDst = 0;

	CUDA_CHECK(cudaMallocHost((void**)&pSrc, sizeof(float) * imgCnt * weightSize));
	CUDA_CHECK(cudaMallocHost((void**)&pDst, sizeof(float) * weightSize));
	
	CUDA_CHECK(cudaMemcpy(pSrc, delta,
		sizeof(float) * imgCnt * weightSize, cudaMemcpyDeviceToHost));
	CUDA_CHECK(cudaMemcpy(pDst, weights,
		sizeof(float) * weightSize, cudaMemcpyDeviceToHost));
		
	for (int i = 1; i < imgCnt; ++i)
	{
		for (int j = 0; j < weightSize; ++j)
		{
			pSrc[j] += pSrc[i * weightSize + j];
		}
	}
	
	for (int i = 0; i < weightSize; ++i)
	{
		pDst[i] += pSrc[i] / (float)imgCnt;
	}
	
	CUDA_CHECK(cudaMemcpy(weights, pDst,
		sizeof(float) * weightSize, cudaMemcpyHostToDevice));
		
	CUDA_CHECK(cudaFreeHost(pSrc));
	CUDA_CHECK(cudaFreeHost(pDst));
}

void UpdateWeights(const NNSIZE &size, const NNWEIGHT &weights, const NNBUFFER &allBuf)
{
	FixWeight(weights.WH, allBuf.deltaWH, size.imgCnt, size.hidSize * size.imgSize);
	FixWeight(weights.BH, allBuf.deltaBH, size.imgCnt, size.hidSize);
	FixWeight(weights.WO, allBuf.deltaWO, size.imgCnt, size.outSize * size.hidSize);
	FixWeight(weights.BO, allBuf.deltaBO, size.imgCnt, size.outSize);
}

void DownloadWeights(const NNSIZE &size, const NNWEIGHT &devW, NNWEIGHT &hostW)
{
	CUDA_CHECK(cudaMemcpy(
			hostW.WH,
			devW.WH,
			size.hidSize * size.imgSize * sizeof(float),
			cudaMemcpyDeviceToHost
			));
	CUDA_CHECK(cudaMemcpy(
			hostW.BH,
			devW.BH,
			size.hidSize * sizeof(float),
			cudaMemcpyDeviceToHost
			));
	CUDA_CHECK(cudaMemcpy(
			hostW.WO,
			devW.WO,
			size.outSize * size.hidSize * sizeof(float),
			cudaMemcpyDeviceToHost
			));
	CUDA_CHECK(cudaMemcpy(
			hostW.BO,
			devW.BO,
			size.outSize * sizeof(float),
			cudaMemcpyDeviceToHost
			));
}

bool Train(const NNSIZE &allSize, const float *pImages, const BYTE *pLables, NNWEIGHT &outW)
{
	int nBatchSize = 50;
	if (allSize.imgCnt % nBatchSize)
	{
		//return false;
	}
	int nBatchs = allSize.imgCnt / nBatchSize;
	int nTestBatchs = nBatchs / 5;
	int nTrainBatchs = nBatchs - nTestBatchs;
	
	NNSIZE size = allSize;
	size.imgCnt = nBatchSize;
	
	NNWEIGHT weights;
	InitWeight(size, weights);
	
	NNBUFFER allBuf;
	InitBuffer(size, allBuf);

	float fLearnRate = 5.0f;
	std::vector<float> maxWH;
	float fMaxPrec = 0.0f;
	for (int i = 0; i < 150; ++i)
	{
		float fCostSum = 0.0f;
		float fTrainPrecSum = 0.0f;
		
		for (int j = 0; j < nTrainBatchs; ++j)
		{
			int nDataIdx = j * nBatchSize;
			FillData(
				size,
			 	pImages + nDataIdx * size.imgSize,
			 	pLables + nDataIdx,
			 	allBuf
			 	);
			
			TrainEpoch<<<size.imgCnt, 1>>>(
				size,
				weights,
				allBuf,
				fLearnRate
				);
	
			float fCost = 0.0f, fPrec = 0.0f;
			Precision(size, allBuf, &fCost, &fPrec);
			
			UpdateWeights(size, weights, allBuf);
			fCostSum += fCost;
			fTrainPrecSum += fPrec;
			
			cudaDeviceSynchronize();
			
		}
		float fTestPrecSum = 0.0f;
		for (int j = nTrainBatchs; j < nBatchs; ++j)
		{
			int nDataIdx = j * nBatchSize;
			FillData(
				size,
				pImages + nDataIdx * size.imgSize,
				pLables + nDataIdx,
				allBuf
				);
				
			TrainEpoch<<<size.imgCnt, 1>>>(
				size,
				weights,
				allBuf,
				fLearnRate
				);

			float fCost, fPrec;
			Precision(
				size,
				allBuf,
				&fCost,
				&fPrec
				);
			
			fTestPrecSum += fPrec;
			cudaDeviceSynchronize();
		}
		fLearnRate *= 0.99f;
		
		fCostSum /= nTrainBatchs;
		fTrainPrecSum /= nTrainBatchs;
		fTestPrecSum /= (nBatchs - nTrainBatchs);
		
		if (fTestPrecSum > fMaxPrec)
		{
			fMaxPrec = fTestPrecSum;
			DownloadWeights(size, weights, outW);
		}
		
		std::cout << "[" << i << "] Cost: " << fCostSum << ", ";
		std::cout << "Train Precision: " << fTrainPrecSum << ", ";
		std::cout << "Test Precision: " << fTestPrecSum << std::endl;
	}
	
	FreeBuffer(allBuf);
	FreeWeight(weights);
	
	return false;
}
