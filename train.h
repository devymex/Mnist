/*
 * cudatest.h
 *
 *  Created on: Sep 17, 2016
 *      Author: devymex
 */

#ifndef MNIST_CUDA_
#define MNIST_CUDA_

#include "nntypes.h"

struct NNSIZE
{
	int imgCnt;
	int imgSize;
	int hidSize;
	int outSize;
};

struct NNBUFFER
{
//Train Data
	float *images;
	BYTE *lables;
	BYTE *results;

//delta
	float *deltaWH;
	float *deltaBH;
	float *deltaWO;
	float *deltaBO;

//Helper
	float *bufHid;
	float *bufSum;
	float *bufOut;
	float *bufErr;

	float *bufCost;
};

struct NNWEIGHT
{
	float *WH;
	float *BH;
	float *WO;
	float *BO;
};

void InitHostWeight(const NNSIZE &size, NNWEIGHT &weights);
void FreeHostWeight(const NNWEIGHT &weights);

bool Train(const NNSIZE &size, const float *pImages, const BYTE *pLables, NNWEIGHT &outW);


#endif /* MNIST_CUDA_ */
