#include <algorithm>
#include <fstream>
#include <iostream>
#include "iohelper.h"

bool LoadImageSet(
		const std::string &strFileName,
		std::vector<float> &imgs,
		int *pImgCnt,
		int *pImgHeight,
		int *pImgWidth
		)
{
	std::ifstream inFile(strFileName, std::ios::binary);
	if (!inFile)
	{
		std::cerr << "Can't Open File: " << strFileName << std::endl;
		return false;
	}

	int header[4];
	if (!inFile.read((char*)&header, sizeof(header)))
	{
		std::cerr << "Bad File Format: " << strFileName << std::endl;
		return false;
	}
	for (int i = 0; i < 4; ++i)
	{
		std::reverse((char*)&(header[i]), ((char*)&(header[i])) + 4);
	}

	if (header[0] != 2051)
	{
		std::cerr << "Bad File Format: " << strFileName << std::endl;
		return false;
	}

	*pImgCnt = header[1];
	*pImgHeight = header[2];
	*pImgWidth = header[3];

	int nImgSize = *pImgHeight * *pImgWidth;
	std::vector<BYTE> imgBuf(*pImgCnt * nImgSize);
	if (*pImgCnt * nImgSize > 0)
	{
		if (!inFile.read((char*)imgBuf.data(), *pImgCnt * nImgSize))
		{
			std::cerr << "Bad File Format!" << std::endl;
			imgs.clear();
			return false;
		}
	}

	imgs.resize(*pImgCnt * nImgSize);
	for (int i = 0; i < *pImgCnt * nImgSize; ++i)
	{
		imgs[i] = (float)imgBuf[i] / 255.0f;
	}
	return true;
}

bool LoadLableSet(const std::string &strFileName, std::vector<BYTE> &lables)
{
	std::ifstream inFile(strFileName, std::ios::binary);
	if (!inFile)
	{
		std::cerr << "Can't Open File: " << strFileName << std::endl;
		return false;
	}

	int header[2];
	if (!inFile.read((char*)&header, sizeof(header)))
	{
		std::cerr << "Bad File Format: " << strFileName << std::endl;
		return false;
	}
	for (int i = 0; i < 2; ++i)
	{
		std::reverse((char*)&(header[i]), ((char*)&(header[i])) + 4);
	}

	if (header[0] != 2049)
	{
		std::cerr << "Bad File Format: " << strFileName << std::endl;
		return false;
	}

	int nLableCnt = header[1];

	if (nLableCnt > 0)
	{
		lables.resize(nLableCnt);
		if (!inFile.read((char*)lables.data(), nLableCnt))
		{
			std::cerr << "Bad File Format!" << std::endl;
			lables.clear();
			return false;
		}
	}
	return true;
}

